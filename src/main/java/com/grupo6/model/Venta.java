package com.grupo6.model;

import java.util.List;

public class Venta {

	private double totalVenta;
	private List<Producto> productos;
	private String fechaVenta;

	public double getTotalVenta() {
		return totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public String getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(String fecha) {
		this.fechaVenta = fecha;
	}

}
