package com.grupo6.model;

public class Producto {

	private String idProducto;
	private String descripcion;
	private Precio precioUnitario;

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Precio getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Precio precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

}
