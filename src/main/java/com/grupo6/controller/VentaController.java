package com.grupo6.controller;

import com.grupo6.business.ProductoBusiness;
import com.grupo6.business.TotalDelDiaBusiness;
import com.grupo6.business.VentaBusiness;
import com.grupo6.model.Precio;
import com.grupo6.model.Producto;
import com.grupo6.model.Venta;
import com.grupo6.view.VentaView;

public class VentaController {

	private VentaBusiness ventaBusiness;
	private ProductoBusiness productoBusiness;
	private TotalDelDiaBusiness totalDelDiaBusiness;
	private VentaView ventaView;

	public VentaController() {

	}

	public void init() {
		this.ventaView.getBtnAgregarProducto().addActionListener(e -> agregarProducto());
		this.ventaView.getBtnCobrar().addActionListener(e -> registrarVenta());
		this.ventaView.mostrar();
	}

	private void registrarVenta() {

		Venta venta = new Venta();
		venta.setProductos(productoBusiness.getProductList());
		venta.setFechaVenta("dd-MM-yyyy");
		venta.setTotalVenta(productoBusiness.getTotalAPagar());

		ventaBusiness.registrarVenta(venta);

		totalDelDiaBusiness.setTotalDelDia(totalDelDiaBusiness.getTotalDelDia() + venta.getTotalVenta());
		productoBusiness.limpiarListaProductos();
		ventaView.getLblVentaTotalValue().setText(totalDelDiaBusiness.getTotalDelDia() + "");
		ventaView.getLblTotalValue().setText("0");
	}

	private void agregarProducto() {
		Producto producto = new Producto();
		producto.setIdProducto("XXX");
		double precioUnitario = Double.parseDouble(ventaView.getInputMonto().getText());

		Precio precio = new Precio();
		precio.setPrecioUnitario(precioUnitario);
		producto.setPrecioUnitario(precio);
		producto.setDescripcion("XXX");

		productoBusiness.agregarProducto(producto);

		ventaView.getLblTotalValue().setText(productoBusiness.getTotalAPagar() + "");
		ventaView.getInputMonto().setText("0");
	}

	public VentaView getVentaView() {
		return ventaView;
	}

	public void setVentaView(VentaView ventaView) {
		this.ventaView = ventaView;
	}

	public VentaBusiness getVentaBusiness() {
		return ventaBusiness;
	}

	public void setVentaBusiness(VentaBusiness ventaBusiness) {
		this.ventaBusiness = ventaBusiness;
	}

	public ProductoBusiness getProductoBusiness() {
		return productoBusiness;
	}

	public void setProductoBusiness(ProductoBusiness productoBusiness) {
		this.productoBusiness = productoBusiness;
	}

	public TotalDelDiaBusiness getTotalDelDiaBusiness() {
		return totalDelDiaBusiness;
	}

	public void setTotalDelDiaBusiness(TotalDelDiaBusiness totalDelDiaBusiness) {
		this.totalDelDiaBusiness = totalDelDiaBusiness;
	}

}
