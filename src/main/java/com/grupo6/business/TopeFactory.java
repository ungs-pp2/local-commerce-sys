package com.grupo6.business;

import com.grupo6.utils.Loader;
import com.grupo6.utils.PropertiesService;

import java.io.IOException;

public class TopeFactory {

    public Tope make() throws IOException {
        String topeconfigurado = (String) new PropertiesService("config/configuracion.properties").readProperties().get("tope");
        Tope tope = new Loader().load(Tope.class).get(0);
        tope.topeDelDia(Double.parseDouble(topeconfigurado));
        return tope;
    }
}
