package com.grupo6.business;

import java.util.ArrayList;
import java.util.List;

import com.grupo6.model.Producto;

public class ProductoBusiness {

	private List<Producto> productList;
	private double totalAPagar;

	public ProductoBusiness (){
		totalAPagar = 0;
		productList = new ArrayList<>();
	}
	
	public void agregarProducto(Producto producto) {
		productList.add(producto);
		totalAPagar += producto.getPrecioUnitario().getPrecioUnitario();
	}

	public void limpiarListaProductos() {
		productList.clear();
		totalAPagar = 0;
	}

	public List<Producto> getProductList() {
		return productList;
	}

	public void setProductList(List<Producto> productList) {
		this.productList = productList;
	}

	public double getTotalAPagar() {
		return totalAPagar;
	}

	public void setTotalAPagar(double totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

}
