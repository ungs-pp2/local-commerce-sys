package com.grupo6.business;

import java.util.LinkedList;
import java.util.List;

import com.grupo6.model.Venta;
import com.grupo6.utils.VentasMapper;

public class VentaBusiness {

	private List<Venta> ventaList;

	private VentasMapper mapper;

	public VentaBusiness(VentasMapper mapper) {
		this.ventaList = new LinkedList<>();
		this.mapper = mapper;
	}

	public void registrarVenta(Venta venta) {
		mapper.write(venta, ventaList);
	}
}
