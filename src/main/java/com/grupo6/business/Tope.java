package com.grupo6.business;

import java.util.Observer;

public interface Tope extends Observer {

    Tope topeDelDia(double topeDelDia);
}
