package com.grupo6.business;

import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public class TotalDelDiaBusiness extends Observable implements Observer {

	private double totalDelDia;

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
	}

	public double getTotalDelDia() {
		return totalDelDia;
	}

	public void setTotalDelDia(double totalDelDia) {
		this.totalDelDia = totalDelDia;
		super.setChanged();
		super.notifyObservers();
	}

}
