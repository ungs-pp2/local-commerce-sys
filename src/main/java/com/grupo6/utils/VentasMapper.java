package com.grupo6.utils;

import java.util.List;

import com.grupo6.model.Venta;

public interface VentasMapper {

	VentasMapper setPath(String path);

	List<Venta> read();

	void write(Venta venta, List<Venta> ventas);
}
