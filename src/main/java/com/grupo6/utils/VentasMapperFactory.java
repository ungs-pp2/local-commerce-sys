package com.grupo6.utils;

import java.io.IOException;

public class VentasMapperFactory {

	private static VentasMapperFactory instance;

	public static VentasMapperFactory getInstance() {
		if(instance == null)
			instance = new VentasMapperFactory();
		return instance;
	}

	public VentasMapper make() throws IOException {
		String path = new PropertiesService("config/configuracion.properties").readProperty("path_persistencia");
		return new Loader().load(VentasMapper.class).get(0).setPath(path);
	}
}
