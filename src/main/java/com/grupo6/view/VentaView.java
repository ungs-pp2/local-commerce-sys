package com.grupo6.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Font;

public class VentaView {

	private JFrame frame;
	private JTextField inputMonto;
	private JButton btnAgregarProducto;
	private JButton btnCobrar;
	private JLabel lblTotalValue;
	private JButton btnGestionarTope;
	private JLabel lblVentaTotalValue;

	/**
	 * Create the application.
	 */

	public VentaView() {
		initialize();
	}

	public void mostrar() {
		frame.setVisible(true);
	}

	public void cerrar() {
		frame.dispose();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBounds(10, 11, 604, 419);
		frame.getContentPane().add(mainPanel);
		mainPanel.setLayout(null);

		JPanel ventaPanel = new JPanel();
		ventaPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		ventaPanel.setBackground(Color.WHITE);
		ventaPanel.setBounds(0, 0, 450, 419);
		mainPanel.add(ventaPanel);
		ventaPanel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Precio del producto:");
		lblNewLabel.setBounds(10, 11, 114, 30);
		ventaPanel.add(lblNewLabel);

		inputMonto = new JTextField();
		inputMonto.setHorizontalAlignment(SwingConstants.RIGHT);
		inputMonto.setBounds(133, 11, 208, 30);
		ventaPanel.add(inputMonto);
		inputMonto.setColumns(10);

		btnAgregarProducto = new JButton("Agregar");
		btnAgregarProducto.setBounds(351, 15, 89, 23);
		ventaPanel.add(btnAgregarProducto);

		JLabel lblTotal = new JLabel("Total");
		lblTotal.setBounds(10, 52, 114, 30);
		ventaPanel.add(lblTotal);

		lblTotalValue = new JLabel("0");
		lblTotalValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTotalValue.setBounds(133, 52, 208, 30);
		ventaPanel.add(lblTotalValue);

		btnCobrar = new JButton("Cobrar");
		btnCobrar.setBounds(252, 93, 89, 23);
		ventaPanel.add(btnCobrar);

		JLabel lblVentasTotales = new JLabel("Ventas totales en el dia:");
		lblVentasTotales.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblVentasTotales.setBounds(10, 363, 192, 45);
		ventaPanel.add(lblVentasTotales);

		lblVentaTotalValue = new JLabel("0");
		lblVentaTotalValue.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblVentaTotalValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVentaTotalValue.setBounds(212, 362, 192, 46);
		ventaPanel.add(lblVentaTotalValue);

		JPanel optionsPanel = new JPanel();
		optionsPanel.setBackground(Color.DARK_GRAY);
		optionsPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		optionsPanel.setBounds(449, 0, 155, 419);
		mainPanel.add(optionsPanel);
		optionsPanel.setLayout(null);

		btnGestionarTope = new JButton("Gestionar Topes");
		btnGestionarTope.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnGestionarTope.setBounds(10, 11, 135, 48);
		optionsPanel.add(btnGestionarTope);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getInputMonto() {
		return inputMonto;
	}

	public void setInputMonto(JTextField inputMonto) {
		this.inputMonto = inputMonto;
	}

	public JButton getBtnAgregarProducto() {
		return btnAgregarProducto;
	}

	public void setBtnAgregarProducto(JButton btnAgregarProducto) {
		this.btnAgregarProducto = btnAgregarProducto;
	}

	public JButton getBtnCobrar() {
		return btnCobrar;
	}

	public void setBtnCobrar(JButton btnCobrar) {
		this.btnCobrar = btnCobrar;
	}

	public JLabel getLblTotalValue() {
		return lblTotalValue;
	}

	public void setLblTotalValue(JLabel lblTotalValue) {
		this.lblTotalValue = lblTotalValue;
	}

	public JButton getBtnGestionarTope() {
		return btnGestionarTope;
	}

	public void setBtnGestionarTope(JButton btnGestionarTope) {
		this.btnGestionarTope = btnGestionarTope;
	}

	public JLabel getLblVentaTotalValue() {
		return lblVentaTotalValue;
	}

	public void setLblVentaTotalValue(JLabel lblVentaTotalValue) {
		this.lblVentaTotalValue = lblVentaTotalValue;
	}

}
