package com.grupo6.main;

import java.io.IOException;
import java.util.ArrayList;

import com.grupo6.business.*;
import com.grupo6.controller.VentaController;
import com.grupo6.model.Venta;
import com.grupo6.utils.PropertiesService;
import com.grupo6.utils.VentasMapper;
import com.grupo6.utils.VentasMapperFactory;
import com.grupo6.view.VentaView;

public class Main {

	public static void main(String[] args) throws IOException {

		VentaView ventaView = new VentaView();
		ProductoBusiness productoBusiness = new ProductoBusiness();

		VentasMapper ventasMapper = VentasMapperFactory.getInstance().make();
		VentaBusiness ventaBusiness = new VentaBusiness(ventasMapper);

		Tope topeObserver = new TopeFactory().make();
		TotalDelDiaBusiness totalDelDiaBusiness = new TotalDelDiaBusiness();
		totalDelDiaBusiness.addObserver(topeObserver);

		VentaController ventaController = new VentaController();
		ventaController.setVentaBusiness(ventaBusiness);
		ventaController.setProductoBusiness(productoBusiness);
		ventaController.setTotalDelDiaBusiness(totalDelDiaBusiness);
		ventaController.setVentaView(ventaView);
		ventaController.init();
	}

}
